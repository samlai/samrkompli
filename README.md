# Interpreter (kompilator do kodu Prolog) języka logicznego o składni podobnej do języka Lojban

# Syntaktyka

Projekt stworzony na kurs Prologa w UWr w 2019 roku. Obecnie rozwijany poza kursem.
Program jest nadal rozwijany więc wciąż nie implementuje całej gramatyki języka Lojban,
a jedynie jego fragment. Pełną składnie w YACC znajdziemy na:
http://lojban.github.io/cll/21/1/
Zaś w EBNF:
http://lojban.github.io/cll/21/2/

Ów fragment opisuje poniższa gramatyka:

```EBNF
text :=
	  sentence
	| sentence ".i" text

sentence :=
	  bridi
	| bridi ".ja nai" sentence-ja

sentence-ja :=
	  sentence-je
	| sentence-je "ja" sentence-ja

sentence-je :=
	  bridi
	| bridi "je" sentence-je

bridi :=
	sumtis ["cu"] selbri sumtis

selbri :=
	  selbri_D
	| selbri_D selbri

selbri_D :=
	  gismu
	| lujvo
	| fuhivla
	| GOhA

sumtis := [] | sumti sumtis

sumti := 
	  sumti_6
	| sumti_6 "poi" bridi

sumti_6 := 
	  "lo'u"  any-word "le'u"
	| LE selbri ["ku"]
	| "ro" selbri
	| "la" alfa ["ku"]
	| "zo" alfa
	| KOhA [free]
	| "li" PA
	| "lu" any-text "li'u"

KOhA := "da" | "de" | "di" | "ko" | "zo\'e"

LE := "le" | "lo"

free := "xi" number

number := PA | PA number

PA := "no"|"pa"|"re"|"ci"|"vo"|"mu"|"xa"|"ze"|"bi"|"so"
     |"0" |"1" |"2" |"3" |"4" |"5" |"6" |"7" |"8" |"9"
```

Ze słownika zostaną wzięte:
 * gismu
 * lujvo
 * fuhivla ( pisze się w lojban: fu'ivla )
Dodatkowo:
 * any-wrod - to dowolne słowo w alfabecie łacińskim
 * alfanum - to dowolne słowo alfanumeryczne w alfabecie łacińskim

# Kompilacja i instalacja

Kod kompilujemy po prostu za pomocą `make` a instalujemy poprzez `make install`.

# Użytkowanie

## Uruchamianie

### W interpreterze

Kod lojban można załadować do interpretera podając go w argumencie.

```command
$ jonsnu file.jbo
```

### Dwuetapowo

Jeżeli chcesz zobaczyć, do czego twój kod został skompilowany, możesz zrobić to w dwóch etapach.

Kod lojban kompilujemy za pomocą `samrkompli`:

```command
$ samrkompli file.jbo -o file.pl
```

Dzięki temu pojawi nam się plik `file.pl`, który można uruchomić z pomocą interpretera `jonsnu`:

```command
$ jonsnu
jonsnu>> ko samymo'i lu file li'u
go'i
```

Gdzie `ko` oznacza rozkaz, `samymo'i` to ładowanie pliku/programu, zaś `lu` i `li'u` wyznaczają cytat.

Słowo `go'i` to tradycyjne lożbańskie potwierdzenie poprzedniego zdania.
W przeciwieństwie do `na go'i` oznaczające zaprzeczenie.

Cytat sam pozbędzie się sam spacji i doda `.pl` na koniec uzyskując `file.pl`.

Można też uruchomić `jonsnu` z argumentem:

```command
$ jonsnu file.pl
```

## Przykład `gerku.jbo`

Weźmy prosty kod o treści:

```lojban
da poi gerku cu gercmo ja nai
  da viska la alis i

lo gerku cu viska la alis
```

Jest w nim napisane:
```
X, który jest psem, szczeka jeżeli
	X widzi Alicję.

Ten pies widzi Alicję.
```

Po skompilowaniu możemy go uruchomić i przetestować prosty sylogizm:

```command
$ samrkompli gerku.jbo
% halt
$ jonsnu
jonsnu>> ko samymo'i lu a li'u
go'i

jonsnu>> da cu gerku
da du lo gerku

go'i
jonsnu>> da cu gercmo
da du lo gerku

go'i
jonsnu>> ko sisti
$ 
```

Kolejne odpowiedzi uzyskujemy za pomocą klawisza `;`.

Można też zamiast tego od razu uruchomić plik `.jbo` z interpretera:

```command
$ jonsnu gerku.jbo
```

Wówczas plik zostanie skompilowany do pliku tymczasowego i załadowany w interpreterze.

## Przykład `lanzu.jbo`

Wykonajmy prosty kod opisujący relacje rodzinne.

Wykorzystamy następujące słowa:

|słowo |											tłumaczenie							|
|------|--------------------------------------------------------------------------------|
|nanmu |x1 to mężczyzna/mężczyźni; x1 to mężczyzna humanoidalny [niekoniecznie dorosły].|
|ninmu |x1 to kobieta/kobiety; x1 jest kobietą humanoidalną [niekoniecznie dorosła].	|
|rirni |x1 jest rodzicem/twórcą/wychowawcą x2; x1 wychowuje x2.							|
|patfu |x1 jest ojcem x2 [niekoniecznie biologiczny].									|
|mamta |x1 jest matką x2 [niekoniecznie biologiczny].									|
|mensi |x1 jest siostrą dla x2 przez rodzica x3 [niekoniecznie biologiczny].			|
|tamne |x1 to kuzyn do x2 przez wiązanie x3 [domyślnie to samo pokolenie].				|
|seldze|x1 jest potomkiem ze starszym x2 z wiązaniem przodków x3; x1 jest potomkiem x2.	|

![lanzu.png]

Na początek wprowadzimy mężczyzn:

```lojban
la john nanmu i
la adam nanmu i
la joshua nanmu i
la david nanmu i
la mark nanmu i
```

Teraz kobiety:

```lojban
la eve ninmu i
la helen ninmu i
la ivonne ninmu i
la anna ninmu i
```

Wprowadźmy informacje kto jest czyim przodkiem:

```lojban
la adam rirni la helen i
la adam rirni la ivonne i
la adam rirni la anna i

la eve rirni la helen i
la eve rirni la ivonne i
la eve rirni la anna i

la john rirni la joshua i
la helen rirni la joshua i

la ivonne rirni la david i
la mark rirni la david i
```

Oraz co znaczy być tatą, czyli mężczyzną będącym rodzicem.

```lojban
da patfu de ja nai
	da nanmu je
	da rirni de i
```

Mamą, czyli kobietą będącą rodzicem.

```lojban
da mamta de ja nai
	da ninmu je
	da rirni de i
```

Siostrą, czyli kobietą o tym samym rodzicu.

```lojban
da mensi de di ja nai
	da ninmu je
	di rirni da je
	di rirni de i
```

Kuzynem/Kuzynką, czyli osobą o tym samym dziadku.

```lojban
da tamne de di xi pa ja nai
	di xi pa rirni di xi re je
	di xi re rirni da je
	di xi pa rirni di xi ci je
	di xi ci rirni de i
```

Potomkiem, czyli mieć rodzica, lub być potomkiem dziecka.

```lojban
da seldze de ja nai
	de rirni da i

da seldze de ja nai
	de rirni di je
	da seldze di
```

Uruchamiamy nasz kod:

```shell
$ jonsnu lanzu.jbo
```

I teraz możemy zadawać pytania do naszej bazy.
Na przykład:

 * Czy John jest potomkiem Marka?

```jonsnu
jonsnu>> la john seldze la mark

na go'i
```

 * Kto jest potomkiem Adama?

```jonsnu
jonsnu>> da seldze la adam
da du la helen

go'i
	ja
da du la ivonne

go'i
	ja
da du la anna

go'i
	ja
da du la joshua

go'i
	ja
da du la david

go'i
	ja

na go'i
```

Kolejne odpowiedzi uzyskujemy za pomocą klawisza `;`.

 * Kto jest siostrą Ivonne?

```jonsnu
jonsnu>> da mensi la ivonne
da du la helen

go'i
	ja
da du la helen

go'i
	ja
da du la ivonne

go'i
	ja
da du la ivonne

go'i
	ja
da du la anna

go'i
	ja
da du la anna

go'i
	ja

na go'i
```

 * Kto ma w tej rodzinie kuzyna i kim ten kuzyn jest?

```jonsnu
jonsnu>> da tamne de
de du la joshua
da du la joshua

go'i
	ja
de du la david
da du la joshua

go'i
	ja
de du la joshua
da du la david

go'i
	ja
de du la david
da du la david

go'i
	ja
de du la joshua
da du la joshua

go'i
	ja
de du la david
da du la joshua

go'i
	ja
de du la joshua
da du la david

go'i
	ja
de du la david
da du la david

go'i
	ja

na go'i
```

# Opis działania

## Zmiana
 - [x] TODO
Kompilator będzie pobierał tekst w lojban po czym zmieniał jego składanie na prolog-ową.
Przykładowo:

```lojban
lo gerku cu tavla la alice
```

"Pies mówi do Alicji"
Zostanie zmienione na coś podobnego do:

```prolog
tavla(lo(gerku),la(alice)).
```

Oczywiście nie będzie to tak wyglądało przez dodanie do kodu
różny elementów potrzebnych w dalszym interpretowaniu.

## Klauzule
Oczywiście język logiczny musi mieć jakieś reguły wnioskowania:

 - [ ] TODO

```lojban
ro gerku cu gercmo gi'a nai viska la alis 
```
Co znaczy "Każdy pies szczeka widząc Alicję".
Co można jeszcze zapisać jako:

 - [x] TODO

```lojban
da poi gerku cu cmoni ja nai
	da viska la alis
```
Czyli "X będący psem szczeka, jeżeli X widzi Alicję"

A w Prolog możemy to zapisać jako:

```prolog
gercmo(X):-
	gerku(X),
	viska(X,la(alis)).
```

Uwaga: Kod prologa ma jedynie charakter poglądowy.

## Typowanie
 - [x] TODO
Semantyka w tym języku pozwala na szybsze opisywanie zdań w kwantyfikatorem ogólnych np:

```lojban
ro gerku cu tavla lu hau hau li'u
```

"Każdy pies mówi 'hau hau' "

```prolog
tavla(X,lu('hau hau')):-
	gerku(X).
```



## Wbudowane polecenia
 - [x] TODO
### Wypisywanie
 - [x] TODO
```lojban
ko ciska lu Hello Word li'u
```

```prolog
write('Hello Word').
```

### Wczytywanie
 - [x] TODO
```lojban
ko tcidu da
```

```prolog
read_jbo(X).
```

## Przecięcia
 - [x] TODO
```lojban
ko katna
```

```prolog
!.
```

## Słownictwo

Słownictwo języka lojban można pobrać w postaci XML za strony:
http://jbovlaste.lojban.org/export/xml.html
po czym przetwarzać poprzez `xpath/3` i dodawać do bazy danych za pomocą `asserta/1` i skompilować.

Plik `lojban_en.xml` należy wkleić do folderu nowo utworzonego folderu `data`.

# TODO
 - [ ] Poprawa semantyki `ro` wewnątrz klauzuli
 - [ ] tanru
 - [ ] GIhE
 - [ ] FA
 - [ ] SE


## Listy
 - [ ] TODO

### Lista pusta
 - [ ] TODO
```lojban
lo kunti liste
```

```prolog
[]
```

### Lista
 - [ ] TODO
```lojban
da du le mlatu ce'o lo gerku ce'o la tricu
```

```prolog
Da = [le(mlatu),lo(gerku),lo(tricu)].
```

### Głowa
 - [ ] TODO
```lojban
da stedu de
```

```prolog
[Da|_] = De.
```

### Ogon
 - [ ] TODO
```lojban
da rebla di
```

```prolog

[_|Da] = Di.
```

### Głowa i ogon
 - [ ] TODO
```lojban
da sterebe de di
```

```prolog
Da = [De,Di].
```

[lanzu.png]: /test/lanzu.png
