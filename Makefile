prefix := /usr/local
INSTALL:= /usr/bin/install

all: samrkompli jonsnu

samrkompli_sources = prolog/samrkompli.pl \
 prolog/gentufa.pl \
 prolog/vlacku.pl

jonsnu_sources = prolog/jonsnu.pl \
 $(samrkompli_sources)

samrkompli: $(samrkompli_sources)
	swipl -t make_samrkompli prolog/samrkompli.pl

jonsnu:  $(jonsnu_sources)
	swipl -t make_jonsnu prolog/jonsnu.pl

install: samrkompli jonsnu
	$(INSTALL) -m 755 samrkompli $(prefix)/bin
	$(INSTALL) -m 755 jonsnu $(prefix)/bin

uninstall:
	rm $(prefix)/bin/samrkompli
	rm $(prefix)/bin/jonsnu

clear:
	rm -f samrkompli
	rm -f jonsnu
	for i in $(ls prolog | grep "~$"); do rm prolog/$i; done
	for i in $(ls test | grep ".pl$"); do rm test/$i  ; done
