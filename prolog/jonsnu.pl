:- module(jonsnu,[
              jonsnu_main/1,
              jonsnu_proc/0,
              make_jonsnu/0
          ]).

:- use_module(samrkompli).
:- use_module(gentufa).
:- use_module(vlacku).

:- volatile make_jonsnu/0.

%!  make_jonsnu is det
%
%   Compiling program to `jonsnu` file.

make_jonsnu :-
    qsave_program(jonsnu,[goal(true),toplevel(main),stand_alone(true)]).

:- volatile jonsnu_main/1.

%!  josnu_main(Args:list) is det
%
%   Run main predicate of jonsnu.

jonsnu_main(A):-
    main(A).

main([N|L]):-
    main_parser(L,Op),
    !,
    main_(N,Op).

main([N|_]):-
    format("~w: parser fail",[N]).

%!  main_(Name:atom,Options:option) is det
%
%   Parsing args of main in jonsnu.

main_(N,Op):-
    option(job(help),Op,interpreter),
    !,
    format("tepilno: ~w [gaftercu'a] datnyvei.jbo...\n",[N]),
    format("gaftercu'a:\n"),
    format(" --help\t\tti ciska lo sidju\n").
main_(_,Op):-
    option(job(interpreter),Op,interpreter),
    !,
    option(load(L),Op,[]),
    load_files_jbo(L),
    format("coi rodo i ti jonsnu gi'e versiio li no no i ti fingubni\n"),
    prompt(_,'jonsnu>> '),
    jonsnu_proc.
main_(_,Op):-
    option(job(task),Op,interpreter),
    !,
    option(load(L),Op,[]),
    option(task(S),Op,""),
    load_files_jbo(L),
    c_string_jbo(S,LL,V),
    run(LL,V,O,_),
    O = false.

%!  main_parser(Args:list,Option:option) is det

main_parser(Args,Option):-
    main_parser(Args,[],Option).

main_parser(['--help'|M],OpIn,OpOut):-
    !,
    merge_options([job(help)],OpIn,OpNext),
    main_parser(M,OpNext,OpOut).

main_parser(['--'|M],OpIn,OpOut):-
    !,
    atomic_list_concat(M,' ',T),
    atom_string(T,SS),
    merge_options([job(task),task(SS)],OpIn,OpOut).

main_parser([F|M],OpIn,OpOut):-
    !,
    select_option(load(L),OpIn,OpNext,[]),
    main_parser(M,[load([F|L])|OpNext],OpOut).

main_parser([],Op,Op).

load_files_jbo([]):-!.
load_files_jbo([P|T]):-
    file_base_name(P,L),
    atom_concat(N,'.jbo',L),!,
    tmp_file(N,File),
    compile_file(P,File),
    load_files_jbo([File|T]).

load_files_jbo([L|T]):-
    (   atom(L)
    ->  (   exists_file(L)
        ->  [L]
        ;   format("File ~w no exist.\n",[L]),fail
        )
    ;   true),
    load_files_jbo(T).

%!  jonsnu_proc is det
%
%   Run jonsnu interpreter.

jonsnu_proc:-
    save_read_jbo(A,V),
    (    A = 'fa\'o',!
    ;    run(A,V,O,T),
         (    true =  O,
              next_proc(T)
         ;    O = false
         ), !,
         jonsnu_proc
    ).

run(A,V,true,T):-
    call_is_det(A,T),
    write_vars(V),
    format("\ngo'i\n").

run(_,_,false,true):-
    format("\nna go'i\n").

call_is_det(X,T):-
    call(X),
    deterministic(T).

next_proc(true):-!.

next_proc(_):-
    get_single_char(S),
    (   S = 13
    ->  true
    ;   S = 59,
        format("\tja\n")
    ->  fail
    ;   format("i mi na jimpe i pe'u la'o xy ; xy e la'o xy enter xy\n"),
        next_proc(_)
    ).

%!  save_read_jbo(L,Vars:List) is det

save_read_jbo(L,V):-
    catch(c_read_jbo(L,V),
          samrkompli_error(T),(
              format("mi na jimpe i\nla ~w nabmi mi ti\n",[T]),
              fail)
         ),!.
save_read_jbo(L,V):-
    save_read_jbo(L,V).

c_read_jbo(LL,V):-
    read_jbo(P),
    c_jbo(P,LL,V).

c_read_jbo('fa\'o',[]).

c_string_jbo(S,LL,V):-
    string_jbo(S,P),
    c_jbo(P,LL,V).

c_jbo(P,LL,V):-
    det_or_throw(compile_lines_text(P,LL,V),samrkompli_error(samrkompli)).

%!  prom_read_jbo(P) is det
%
%   Same like read_jbo/1 but witch prompt `> `.
%   It is use by `ko tcidu da`.

prom_read_jbo(lu_(P)):-
    prompt(L,'> '),
    catch(read_jbo(P),
          samrkompli_error(T),(
              format("mi na jimpe i\nla ~w nabmi mi ti\n",[T]),
              prompt(_,L),
              fail)
         ),
    prompt(_,L).

%!  read_jbo(P) is det
%
%   It read one line of text and phrase it.

read_jbo(P):-
    read_string(user_input, "\n", "\r", 10, K),
    string_jbo(K,P).

string_jbo(K,P):-
    string_codes(K,CC),
    det_or_throw(phrase(tokenizer(T),CC),samrkompli_error(vlacku)),
    det_or_throw(phrase(text(P),T),samrkompli_error(gentufa)),
    findall(generate_semantic(S,N),(samrkompli:find_term(P,[selbri(S1,N1)|R]),
                                    samrkompli:selbri_atom([selbri(S1,N1)|R],S),
                                    samrkompli:args_of_selbri([selbri(S1,N1)|R],N)
                                   ),M),
    maplist(call,M).

%!  write_jbo(L) is det
%
%   Pritty print lojban structures.

write_jbo([A]):-
    !,
    write_jbo(A).
write_jbo([A,B|T]):-
    !,
    write_jbo(A),
    write(" i "),
    write_jbo([B|T]).
write_jbo(lu(W)):-
    !,
    atomic_list_concat(W,' ',T),
    format("lu ~w li'u",[T]).
write_jbo(lu_(P)):-
    !,
    write("lu "),
    write_jbo(P),
    write(" li'u").
write_jbo(xi(A,N)):-
    !,
    write_jbo(A),
    write(" xi "),
    write_jbo(N).
write_jbo(koha(A)):-
    !,
    write(A).
write_jbo(selbri(T,_)):-
    !,
    write(T).
write_jbo(bridi(T,[A|L])):-
    !,
    write_jbo(A),
    write(" cu "),
    write_jbo(T),
    write(" "),
    write_jbo_(L).
write_jbo(la(X)):-
    !,
    write("la "),
    write_jbo(X).
write_jbo(lo(X)):-
    !,
    write("lo "),
    write_jbo(X).
write_jbo(X):-
    write(X).

write_jbo_([]):-!.
write_jbo_(A):-
    write_jbo_k(A).

write_jbo_k([A]):-
    !,
    write_jbo(A).
write_jbo_k([A,B|T]):-
    write_jbo(A),
    write(" "),
    write_jbo_k([B|T]).

%!  write_vars(L:list) is det
%
%   Pritty write list of vars

write_vars([]):-!.
write_vars([_=V|B]):-
    var(V),!,
    write_vars(B).
write_vars([N=V|B]):-
    map_name(W,N),!,
    write_jbo(W),
    format(" du "),
    write_jbo(V),
    format("\n"),
    write_vars(B).
write_vars([_|B]):-
    write_vars(B).

%!  map_name(-Struct,Atom) is det

map_name(da,'DA'):-!.
map_name(de,'DE'):-!.
map_name(di,'DI'):-!.
map_name(mo,'MO'):-!.
map_name(xi(da,N),CC):-
    atom_concat('DA',K,CC),!,
    atom_number(K,N).
map_name(xi(de,N),CC):-
    atom_concat('DE',K,CC),!,
    atom_number(K,N).
map_name(xi(di,N),CC):-
    atom_concat('DI',K,CC),
    atom_number(K,N).

:- dynamic mo_clause/2.

:- dynamic ciska/4.

ciska(ko,T,'zo\'e','zo\'e'):-
    writeln(T).

:- dynamic 'samymo\'i'/2.

'samymo\'i'(ko,F):-
    atom_concat(F,'.pl',FF),
    load_files([FF]).
