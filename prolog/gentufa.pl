/** <module>  Simple parser of lojban
 *
 * It phrase list of tokens from module vlacku.
 *
 * @see vlacku
 * @author Samuel Li
 */
:- module(gentufa,[
              text//1,
              sentence//1
          ]).

%!  text(Bridis)//
%
%   Phrase tokenized lojban text.
%   ```
%   text :=
%	  sentence
%	| sentence ".i" text
%   ```

text([S|T]) --> sentence(S),([cmavo(i,_)], text(T);{T=[]}).

%!  sentence(Sentence)//
%
%   ```
%   sentence :=
%	  bridi
%	| bridi ".ja nai" sentence-ja
%   ```

sentence(L) --> bridi(H), ([cmavo(ja,_)],[cmavo(nai,_)],!,sentence_ja(T),{L=janai(H,T)};{L=H}).

%!  sentence_ja(Sentence_tail)//
%
%   ```
%   sentence-ja :=
%	  sentence-je
%	| sentence-je "ja" sentence-ja
%   ```

sentence_ja(T) --> sentence_je(A), ([cmavo(ja,_)],!,sentence_ja(B),{T=ja(A,B)};{T=A}).

%!  sentence_je(Sentence_tail)//
%
%   ```
%   sentence-je :=
%	  bridi
%	| bridi "je" sentence-je
%   ```

sentence_je(T) --> bridi(A), ([cmavo(je,_)],!,sentence_je(B),{T=je(A,B)};{T=A}).

%!  bridi(Bridi)//
%
%   ```
%   bridi :=
%	sumtis ["cu"] selbri sumtis
%   ```

bridi(bridi(S,[A|L])) --> sumti(A),bridi_(bridi(S,L)).
bridi(bridi(S,[koha('zo\'e')|L])) --> bridi_(bridi(S,L)).

bridi_(bridi(S,L)) --> ignore([cmavo(cu,_)]), selbri(S), bridi_(bridi(S,L)).
bridi_(bridi(S,[A|L])) --> sumti(A),bridi_(bridi(S,L)).
bridi_(bridi(_,[])) --> [].

%!  selbri(Selbri)//
%
%   ```
%   selbri :=
%	  selbri_D
%	| selbri_D selbri
%   ```

selbri([S|SS]) --> selbri_D(S), selbri(SS).
selbri([S]) --> selbri_D(S).

%!  selbri_D(Selbri)//
%
%   ```
%   selbri_D :=
%	  gismu
%	| lujvo
%	| fuhivla
%	| GOhA
%   ```

selbri_D(selbri(S,N)) --> [gismu(S,N)],!.
selbri_D(selbri(S,N)) --> [lujvo(S,N)],!.
selbri_D(selbri(S,N)) --> [fuhivla(S,N)],!.
selbri_D(goha(G)) --> [cmavo(G,'GOhA')].

%!  sumtis(Sumtis:list)//
%
%   ```
%   sumtis := [] | sumti sumtis
%   ```

sumtis([A|B]) --> sumti(A),sumtis(B).
sumtis([]) --> [].

%!  sumti(Sumti)//
%
%   ```
%   sumti :=
%	  sumti_6
%	| sumti_6 "poi" bridi
%   ```

sumti(T) --> sumti_6(S),([cmavo(poi,_)],!, bridi(E),{T=poi(S,E)};{T=S}).

%!  sumti_6(Sumti)//
%
%   ```
%   sumti_6 :=
%	  "lo'u"  any-word "le'u"
%	| LE selbri ["ku"]
%	| "ro" selbri
%	| "la" alfa ["ku"]
%	| "zo" alfa
%	| KOhA [free]
%	| "li" PA
%	| "lu" any-text "li'u"
%   ```

sumti_6(li(A)) --> [cmavo(li,_)],number(A).
sumti_6(lu(A)) --> [cmavo(lu,_)],sumti_6_lu(A).
sumti_6(T) --> le(L),selbri(S),ignore([cmavo(ku,_)]),{T=..[L,S]}.
sumti_6(la(A)) --> [cmavo(la,_)],tok_name(A),ignore([cmavo(ku,_)]).
sumti_6(K) --> free(K,koha(A),koha(A)).
sumti_6(ro(S)) --> [cmavo(ro,_)],selbri(S).

sumti_6_lu([]) --> [cmavo('li\'u',_)].
sumti_6_lu([A|B]) --> tok_name(A),sumti_6_lu(B).

%!  koha(KOhA)//
%
%   ```
%   KOhA := "da" | "de" | "di" | "ko" | "zo\'e"
%   ```

koha(da) --> [cmavo(da,_)],!.
koha(de) --> [cmavo(de,_)],!.
koha(di) --> [cmavo(di,_)],!.
koha(ko) --> [cmavo(ko,_)],!.
koha('zo\'e') --> [cmavo('zo\'e',_)].

%!  le(LE)//
%
%   ```
%   LE := "le" | "lo"
%   ```

le(le) --> [cmavo(le,_)].
le(lo) --> [cmavo(lo,_)].

tok_name(A) --> [cmavo(A,_)].
tok_name(A) --> [gismu(A,_)].
tok_name(A) --> [lujvo(A,_)].
tok_name(A) --> [fuhivla(A,_)].
tok_name(A) --> [alfa(A)].
tok_name('.') --> [denpa_bu].

%!  free(Out,ToModify,Def)//
%
%   It take phrase To Modify and add to it "xu" modifier.
%
%   ```
%   free := "xi" number
%   ```
%   @see sumti_6//1

:- meta_predicate free(?,//,?,?,?).

free(T,A,C) --> A,([cmavo(xi,_)],!,number(N),{T=xi(C,N)};{T=C}).

denpa_bu --> [denpa_bu].

%!  number(PA)//
%
%   ```
%   number := PA | PA number
%
%   PA := "no"|"pa"|"re"|"ci"|"vo"|"mu"|"xa"|"ze"|"bi"|"so"
%        |"0" |"1" |"2" |"3" |"4" |"5" |"6" |"7" |"8" |"9"
%   ```

number(A) --> number_(0,A).

number_(A,K) --> [pa(N)],!,{number_concat(A,N,C)},number_(C,K).
number_(A,A) --> [].

ignore(A) --> A.
ignore(_) --> [].

number_concat(A,B,N):-
    B==0,!,
    N is A*10.
number_concat(A,B,N):-
    var(N),!,
    N is A*10**floor(log10(B)+1)+B.
