:- module(samrkompli,[compile_file/1,
                      compile_file/2,
                      compile_lines_text/3,
                      compile_lines_text/4,
                      samrkompli_main/1,
                      make_samrkompli/0,
                      generate_semantic/2,
                      find_term/2,
                      det_or_throw/2,
                      unificator_jbo/2,
                      new_zohe/1
                     ]).

% Lexer
:- use_module(vlacku).
% Parser
:- use_module(gentufa).

%!  make_samrkompli
%
%   Compile compiler by `qsave`.

:- volatile make_samrkompli/0.

make_samrkompli :-
    qsave_program(samrkompli,[goal(true),toplevel(main),stand_alone(true)]).

:- volatile samrkompli_main/1.

samrkompli_main(F):-
    main(F).

main([N|L]):-
    main_(N,L,'a.pl',_,nohelp),!.

main([N|_]):-
    format("usage: ~w file_to_compile.jbo",[N]).

%!  main_(Name,Args,Output,File,Help) is det
%
%   Parsing args.

main_(N,[],_,_,help):-
    !,
    format("tepilno: ~w [gaftercu'a] datnyvei.jbo\n",[N]),
    format("gaftercu'a:\n"),
    format(" -o bartu.pl\tle bartu datanyvei cu datnyvei ti\n"),
    format(" --help\t\tti ciska lo sidju\n").
main_(_,[],O,F,_):-
    atom(F),
    (   exists_file(F)
    ->  compile_file(F,O)
    ;   format("File ~w not exist",[F]),!,
        fail
    ).
main_(N,['-o',O|M],_,F,H):-
    !,
    main_(N,M,O,F,H).
main_(N,['--help'|M],O,F,_):-
    !,
    main_(N,M,O,F,help).
main_(N,[F|M],O,_,H):-
    main_(N,M,O,F,H).

%!  compile_file(File) is det
%
%   @see compile_file/2.

compile_file(F):-
    det_or_throw(exists_file(F),samrkompli_error(no_file(F))),
    compile_file(F,'a.pl').

%!  compile_file(File,Output) is det
%
%   Compile lojban `File` to prolog `Output` file.

compile_file(F,O):-
    det_or_throw(phrase_from_file(tokenizer(T),F),samrkompli_error(vlacku)),
    det_or_throw(phrase(text(P),T),samrkompli_error(gentufa)),
    det_or_throw(compile(P,W),samrkompli_error(samrkompli)),
    findall(RR,(find_term(P,lo([selbri(S,N)|R])), lo_clausules([selbri(S,N)|R],RR)),WW,W),
    findall(K,(find_term(P,[selbri(S1,N1)|R]),
               samrkompli:selbri_atom([selbri(S1,N1)|R],S),
               samrkompli:args_of_selbri([selbri(S1,N1)|R],N),
               K = (:-generate_semantic(S,N))
              ),M,WW),
    det_or_throw(print_to_file(O,M),samrkompli_error(ciska(O))).

%!  compile(Structure,Prolog) is det
%
%   Compile parsied structures to porolog code.

compile([],[]).
compile([A|T],AA) :-
    compile_itr(A,AA,TT),
    compile(T,TT).

compile_itr(A,V,K):-
    compile_sentence(A,V,K).

compile_sentence(B,[:-(T)|E],E):-
    is_imperative(B),
    compile_horn_tail(B,T,[],_).

compile_sentence(bridi(S,A),[KK:-TT|E],E):-
    compile_bridi(head,bridi(S,A),K,R,true,[],Y),
    compile_horn_tail(R,TT,Y,_),
    hide(K,KK).

compile_sentence(janai(B,H),[KK:-TT|E],E):-
    compile_bridi(head,B,K,T,H,[],Y),
    compile_horn_tail(T,TT,Y,_),
    hide(K,KK).

hide(A,B):-
    A =.. [AA|G],
    atom_concat(AA,'_',BB),
    B =.. [BB|G].

compile_lines_text(A,L,W):-
    compile_lines_text(A,L,[],W).

compile_lines_text([A],L,V,W):-
    compile_horn_tail(A,L,V,W).
compile_lines_text([A|B],(L,LL),V,W):-
    compile_horn_tail(A,L,V,E),
    compile_lines_text(B,LL,E,W).

compile_horn_tail(true,true,V,V).
compile_horn_tail(je(A,B),(F,G),V,W):-
    compile_horn_tail(A,F,V,Y),
    compile_horn_tail(B,G,Y,W).
compile_horn_tail(ja(A,B),(F;G),V,W):-
    compile_horn_tail(A,F,V,Y),
    compile_horn_tail(B,G,Y,W).
compile_horn_tail(B,(F,L),V,W):-
    compile_bridi(tail,B,F,K,true,V,Y),
    compile_horn_tail(K,L,Y,W).

%!  compilet_bridi(Bridi,Complet_Bridi) is det
%
%   Add unused args to bridi.

complet_bridi(bridi(selbri(S,_),_),_):-
    var(S),
    throw(samrkompli_error(no_selbri)).

complet_bridi(bridi(SS,R),bridi(SS,J)):-
    args_of_selbri(SS,N),
    length(R,NN),
    K is N - NN,
    zohe_tail(W,K),
    append(R,W,J).

%!  args_of_selbri(Selbri,Number) is det

args_of_selbri(T,N):-
    last(T,selbri(_,N)).

%! compile_bridi(Bridi,Prolog,SentOut,SentIn,VarsIn,VarsOut) is det
compile_bridi(_,pl(P),P,O,O,V,V):-!.

compile_bridi(_,bridi(goha(mo),R),mo_clause(MO,SS),D,O,V,Y):-
    compile_sumtis(tail,R,SS,D,O,V,W),
    var_add(W,'MO',MO,Y).

compile_bridi(_,B,T,Q,O,V,W):-
    is_imperative(B),
    complet_bridi(B,BB),
    imper(BB,T,Q,O,V,W).

compile_bridi(Type,B,T,D,O,V,Y):-
    complet_bridi(B,bridi(SS,R)),
    selbri_atom(SS,S),
    compile_sumtis(Type,R,W,D,O,V,Y),
    T =..[S|W].

%!  imper(Struct,Imperative,SentenceOut,SentenceIn,VarIn,VarOut) is det
%
%   It compile imperative structures, to prolog if it is possible.

imper(bridi([selbri(katna,3)],[koha(ko)|_]),!,O,O,V,V).
imper(bridi([selbri(tcidu,_)],[koha(ko),DD|_]),prom_read_jbo(D),Q,O,V,W):-
    compile_sumti(tail,DD,D,Q,O,V,W).
imper(bridi([selbri(ciska,_)],[koha(ko),DD|_]),write(D),Q,O,V,W):-
    compile_sumti(tail,DD,D,Q,O,V,W).
imper(bridi([selbri(sisti,_)],[koha(ko)|_]),halt,O,O,V,V).

%!  is_imperative(A) is semidet

is_imperative(bridi(_,T)):-
    is_imperative_(T).

is_imperative_([koha(ko)|_]):-!.
is_imperative_([_|T]):-
    is_imperative_(T).


compile_sumtis(_,[],[],O,O,V,V).
compile_sumtis(Typ,[A|T],[B|L],D,O,V,Y):-
    compile_sumti(Typ,A,B,D,G,V,W),
    compile_sumtis(Typ,T,L,G,O,W,Y).

compile_sumti(_,D,D,O,O,V,V):-
    var(D),!.
compile_sumti(_,xi(koha(D),N),Y,O,O,V,W):-
    atom_string(D,S),
    string_upper(S,SS),
    string_concat(SS,N,SN),
    atom_string(DD,SN),
    var_add(V,DD,Y,W).
compile_sumti(_,koha(ko),ko,O,O,V,V):-!.
compile_sumti(tail,koha('zo\'e'),'zo\'e',O,O,V,V):-!.
compile_sumti(head,koha('zo\'e'),ZOHE,je(pl(new_zohe(ZOHE)),O),O,V,V):-!.
compile_sumti(_,koha(D),Y,O,O,V,W):-
    atom_string(D,S),
    string_upper(S,SS),
    atom_string(DD,SS),
    var_add(V,DD,Y,W).
compile_sumti(Type,poi(D,bridi(S,[koha('zo\'e')|T])),DD,W,O,V,Y):-
    compile_sumti(Type,D,DD,W,je(bridi(S,[D|T]),O),V,Y).
compile_sumti(_,ro(SS),Y,je(bridi(SS,[Y]),O),O,V,W):-
    new_var(V,_,Y,W).
compile_sumti(_,li(N),N,O,O,V,V).
compile_sumti(_,lo(SS),lo(A),O,O,V,V):-
    selbri_atom(SS,A).
compile_sumti(_,la(S),la(S),O,O,V,V).
compile_sumti(_,lu(W),T,O,O,V,V):-
    atomic_list_concat(W,' ',T).

%!  selbri_atom(Selbri,Atom) is det

selbri_atom(SS,A):-
    maplist([selbri(S,_),S]>>true,SS,AA),
    atomic_list_concat(AA,'_',A).

:- meta_predicate generate_semantic(:,?).

generate_semantic(W:S,N):-
    W:current_predicate(S/N),!.

generate_semantic(W:S,N):-
    W:dynamic(S/N),
    atom_concat(S,'_',SS),
    multifile(SS/N),
    W:multifile(SS/N),
    W:discontiguous(SS/N),
    W:dynamic(SS/N),
    zohe_clausules(S,N,Z),
    maplist(W:assertz,Z),
    mo_clausules(S,N,MO),
    W:asserta(MO).

mo_clausules(S,N,mo_clause(S,X):-T):-
    length(X,N),
    T=..[S|X].

lo_clausules(SS,Q):-
    selbri_atom(SS,S),
    args_of_selbri(SS,N),
    lo_clausules(S,N,Q).

lo_clausules(S,N,W:-true):-
    atom_concat(S,'_',SS),
    un_zohe_tail([_|M],N),
    W =.. [SS,lo(S)|M].

zohe_clausules(S,N,W) :-
    length(Arg,N),
    atom_concat(S,'_',SS),
    findall((T:-LL),
            (
                un_zohe(Arg,KT,MM,R),
                atom_concat(SS,R,SSR),
                T =.. [SSR|Arg],
                succ(R,RR),
                atom_concat(SS,RR,SSRR),
                K=..[SSRR|KT],
                (  LL = (MM,K);
                   LL =.. [SSRR|Arg]
                )
            ),
            W,
            [
                TH:-TL,OH:-OL
            ]),
    atom_concat(SS,0,SS0),
    atom_concat(SS,N,SSN),
    TH =.. [S|Arg],
    TL =.. [SS0|Arg],
    OH =.. [SSN|Arg],
    OL =.. [SS|Arg].

un_zohe(A,B,K,N):-
    un_zohe(A,B,K,0,N).

un_zohe([A|T],[_|T],(A == 'zo\'e',!),N,N).
un_zohe([H|AT],[H|BT],K,N,W):-
    succ(N,M),
    un_zohe(AT,BT,K,M,W).


arg_tail([],0).
arg_tail([A|L],N):-
    N > 0,
    succ(NN,N),
    C is 64+N,
    atom_codes(A,[C]),
    arg_tail(L,NN).

crosso([],[],[]).
crosso([A|T],[_|L],[A|W]):-
    crosso(T,L,W).
crosso([_|T],[B|L],[B|W]):-
    crosso(T,L,W).

list_to_bracket([A|T],(A,B)):-
    list_to_bracket(T,B).
list_to_bracket([A],A).

empty_braket_queue(empty).
uno_braket_queue(uno(_)).

push_front(empty,A,uno(A)):-!.
push_front(uno(E),A,bqu((A,K),K,E)):-!.
push_front(bqu(S,K,E),A,bqu((A,S),K,E)).

push_end(empty,A,uno(A)):-!.
push_end(uno(E),A,bqu((E,K),K,A)):-!.
push_end(bqu(S,K,E),A,bqu(S,R,A)):-
    K = (E,R).

bqu_end(empty,true):-!.
bqu_end(uno(A),A):-!.
bqu_end(bqu(S,A,A),S).

bqu_concat(empty,A,A):-!.
bqu_concat(A,empty,A):-!.
bqu_concat(uno(A),B,C):-
    push_front(B,A,C),!.
bqu_concat(A,uno(B),C):-
    push_front(A,B,C),!.
bqu_concat(bqu(A,(P,B),P),bqu(B,L,R),bqu(A,L,R)):-!.


print_to_file(F,K):-
    open(F,write,S),
    pprint_str(S,K),
    close(S).

pprint_str(S,K):-
    maplist({S}/[R]>>portray_clause(S,R),K).

un_zohe_tail([],0).
un_zohe_tail(['zo\'e'|T],N):-
    N > 0,
    succ(NN,N),
    un_zohe_tail(T,NN).

zohe_tail([],0):-!.
zohe_tail([koha('zo\'e')|T],N):-
    N > 0,
    succ(NN,N),
    zohe_tail(T,NN).

%!  det_or_throw(Term,Throw)
%
%   It do Term only one time or if it fail
%   It throws error.

:- meta_predicate det_or_throw(0,+).

det_or_throw(A,_):-
    call(A),!.
det_or_throw(_,T):-
    throw(T).

%!  unificator_jbo(Sumti1,Sumti2) is det
%
%   It is special unificator for lojban.
%
%   It don't unificable 'zo\'e' and 'zo\'e' because
%   "something" can be meaning another thing than other "something".

unificator_jbo(A,B):-
    var(A),
    !,
    A=B.
unificator_jbo(A,B):-
    var(B),
    !,
    A=B.
unificator_jbo('zo\'e','zo\'e'):-
    !,
    fail.
unificator_jbo(A,A).

%!  new_zohe(-NewZohe) is det
%
%   Return new zo'e witch new index.

:- dynamic zohe_num/1.
zohe_num(0).

new_zohe(xi('zo\'e',N)):-
    zohe_num(N),
    succ(N,NN),
    retract(zohe_num(N)),
    asserta(zohe_num(NN)),!.

%!  var_add(In:list, Name:atom, Var:var, Out:list) is det
%
%   Adding to var witch name for variable_names/1 in write_term/2.

var_add(T,N,V,T):-
    member(N=V,T),!.
var_add(T,N,V,[N=V|T]).

%!  new_var(In:list, Name:atom, Var:var, Out:list) is det
%
%   Generate random name and add it to var list.

new_var(T,N,V,[N=V|T]):-
    random_var_name(N),
    \+ member(N=_,T),!.

random_var_name(A):-
    random_var_name_code(F),
    atom_codes(A,F).

random_var_name_code([N|T]):-
    random_up_code(N),
    random_var_name_code_(T).
random_var_name_code(N):-
    random_var_name_code(N).

random_var_name_code_([]):-
    random(A),
    A < 0.25,!.
random_var_name_code_([C|T]):-
    random_up_code(C),
    random_var_name_code_(T).

random_up_code(C):-
    random(R),
    C is floor(R*26+65).

find_term(V,_):-
    var(V),!,fail.
find_term(A,A).
find_term(T,A):-
    nonvar(T),
    T =.. [_|K],
    find_term_(K,A).

find_term_([T|_],A):-
    find_term(T,A).
find_term_([_|L],A):-
    find_term_(L,A).
