/** <module> Simplex lexer for lojban
 *
 * It lexing list of String codes by tekenizer//1.
 *
 * @author Samuel Li
 */
:-module(vlacku,[
	     tokenizer//1,
	     cmavo/2,
	     cmavo/3,
	     cmavo_FA/2,
	     cmavo_SE/2
	 ]).

%!	tokenizer(Tokens)//
%
%	It is lexing to list of tokens from string codes.

tokenizer(A) --> whites,tokenizer_(A).
tokenizer_([denpa_bu|B]) --> ".",(!,tokenizer_(B);whites,{B=[]}).
tokenizer_([A|B]) --> token(A),(lek_white,whites,!,tokenizer_(B);whites,{B=[]}).
tokenizer_([]) --> whites.

%!	token(Token)//
%
%	It reads token.

token(denpa_bu) --> ".".
token(pa(N)) --> digit_PA1(pa(N)).
token(cmavo(A,S)) --> cmavo_par(A,S).
token(gismu(A,N)) --> gismu_par(A,N).
token(lujvo(A,N)) --> lujvo_par(A,N).
token(fuhivla(A,N)) --> fuhivla_par(A,N).
token(alfa(A)) --> alfa(A).

alfa(A) --> alfa_(AA),{atom_codes(A,AA)}.
alfa_([A|B]) --> alfa_char(A),!,(alfa_(B);{B=[]}).

alfa_char(A) --> [A],{between(97,122,A);between(65,90,A)}.

whites -->  lek_white,!,whites.
whites --> [].
lek_white --> [9],!.
lek_white --> [10],!.
lek_white --> [32].

%!	cmavo_FA(FA,Number)
%
%	It gives information about FA semantic.

cmavo_FA(fa,1).
cmavo_FA(fe,2).
cmavo_FA(fo,4).
cmavo_FA(fu,5).

%!	cmavo_SE(SE,Number)
%
%	It gives information about SE semantic.

cmavo_SE(se,2).
cmavo_SE(te,3).
cmavo_SE(ve,4).
cmavo_SE(xe,5).

%!	cmavo_data(Cmavo,Gramma,Informations)

:- dynamic cmavo_data/3.

%!	cmavo_par(Cmavo,Selmaho)//

:- dynamic cmavo_par//2.

%!	cmavo(Word,Grammatic)
%
%	It gives pairs of Word and his Grammatical class.
%
%       @see cmavo/3

cmavo(A,B):-
	cmavo(A,B,_).

%!	cmavo(Word,Gramatic,Information)
%
%	It gives tuples of words, grammatical classes and information about this word.

cmavo(A,BB,D):-
	is_list(BB),
	cmavo_data(A,B,D),!,
	member(B,BB).

cmavo(A,B,D):-
	cmavo_data(A,B,D).

cmavo(A,BB,D):-
	is_list(BB),!,
	member(B,BB),
	cmavo(A,B,D).

% PA1
cmavo(N,'PA1',[number(N)]):-
	number(N).

%!	digit_PA1(PA)// is semidet
%
%	```
%	PA := "no"|"pa"|"re"|"ci"|"vo"|"mu"|"xa"|"ze"|"bi"|"so"
%	     |"0" |"1" |"2" |"3" |"4" |"5" |"6" |"7" |"8" |"9"
%       ```

digit_PA1(pa(0)) --> "0",!.
digit_PA1(pa(0)) --> "no",!.
digit_PA1(pa(1)) --> "1",!.
digit_PA1(pa(1)) --> "pa",!.
digit_PA1(pa(2)) --> "2",!.
digit_PA1(pa(2)) --> "re",!.
digit_PA1(pa(3)) --> "3",!.
digit_PA1(pa(3)) --> "ci",!.
digit_PA1(pa(4)) --> "4",!.
digit_PA1(pa(4)) --> "vo",!.
digit_PA1(pa(5)) --> "5",!.
digit_PA1(pa(5)) --> "mu",!.
digit_PA1(pa(6)) --> "6",!.
digit_PA1(pa(6)) --> "xa",!.
digit_PA1(pa(7)) --> "7",!.
digit_PA1(pa(7)) --> "ze",!.
digit_PA1(pa(8)) --> "8",!.
digit_PA1(pa(8)) --> "bi",!.
digit_PA1(pa(9)) --> "9",!.
digit_PA1(pa(9)) --> "so".

:- use_module(library(sgml)).
:- use_module(library(pcre)).

%!	dictionary(Dictionary) is det
%
%	The dictionary is stored here.

:- dynamic dictionary/1.
:- volatile dictionary/1.

%!	file_dictionary(-Directiory) is multi
%
%	It is information where program will look for the dictionary.

:- volatile file_dictionary/1.

file_dictionary('.').
file_dictionary('./data/lojban_en.xml').
file_dictionary('../data/lojban_en.xml').
file_dictionary('/opt/jbopl/data/lojban_en.xml').

%!	load_xml_jbo_dictionary is det
%
%	Load xml file of lojban dictionary and phrase it.
%
%	If it is impossible it `halt` the program.

:- volatile load_xml_jbo_dictionary/0.

load_xml_jbo_dictionary:-
	file_dictionary(FD),
	exists_file(FD) ->
	load_xml(FD,Term, []),
	retractall(dictionary(_)),!,
	asserta(dictionary(Term)),
	compile_predicates([dictionary/1]);
	writeln('Please, download lojban_en.xml form http://jbovlaste.lojban.org/export/xml.html'),
	writeln('And put it to data/ or /opt/jbopl/data/'),
	halt(2).
:- load_xml_jbo_dictionary.
:- use_module(library(xpath)).

:- volatile cmavo_d/3.

cmavo_d(Word,Selmaho,Data):-
	member(Class,[cmavo,'cmavo-compound','experimental cmavo']),
	dictionary_record(Word,Class,Record),
	cmavo_get_data(Record,Selmaho,Data).

:- volatile cmavo_get_data/3.

cmavo_get_data(Record,Selmaho,Data):-
	xpath(Record,selmaho,element(selmaho,[],[Selmaho])),
	xpath(Record,user/username(content),[Sampli]),
	xpath(Record,definition(content),[Vlavelcki]),
	analize_definition(Vlavelcki,Selmaho,Op),
	merge_options(Op,[sampli(Sampli),vlavelcki(Vlavelcki)],Data),!.

%!	analize_definition(Word,Gramma,Informaations)
%
%	It reads some informations from definition of Word.
%	Like Gramma class.

:- volatile analize_definition/3.

analize_definition(D,'BY2',[lerfu(K)]):-
	re_matchsub("letteral for (?<lerfu>\\S)",D,S,[]),
	atom_string(K,S.lerfu).
analize_definition(D,'FA',[punji(K)]):-
	re_matchsub("sumti place tag: tag (?<punji>\\d*)\\S+ sumti place.",D,S,[]),
	number_string(K,S.punji).
analize_definition(D,'SE',[punji(K)]):-
	re_matchsub("(?<punji>\\d*)\\S+ conversion; switch 1st/\\d*\\S+ places.",D,S,[]),
	number_string(K,S.punji).
analize_definition(D,'PA1',[namcu(K)]):-
	re_matchsub("digit/number: (?<namcu>\\d*)",D,S,[]),
	number_string(K,S.namcu).

analize_definition(_,_,[]).

%!	cmevla_data(Cmevla)

:- dynamic cmevla_data/1.

%!	cmevla_per(Cmevla)//

:- dynamic cmevla_per//1.

%!	cmevla_d(Cmevla)

:- volatile cmevla_d/1.

cmevla_d(Word):-
	dictionary_record(Word,cmevla,_).

%!	gismu_data(?Gisum,?Arguments)

:- dynamic gismu_data/3.

%!	gismu_par(?Gisum,?Arguments)//

:- dynamic gismu_par//2.

%!	gismu_d(?Gisum,?Arguments,?RafsiList)

:- volatile gismu_d/3.

gismu_d(Gismu,Number,Rafsi):-
	dictionary_selbri(Gismu,gismu,Number,Rafsi,_).
gismu_d(Gismu,Number,Rafsi):-
	dictionary_selbri(Gismu,'experimental gismu',Number,Rafsi,_).

%!	lujvo_data(Lujvo,Arguments)

:- dynamic lujvo_data/2.

%!	lujvo_par(Lujvo,Arguments)//

:- dynamic lujvo_par//2.

%!	lujvo_d(?Lujvo,?Arguments)

:- volatile lujvo_d/2.

lujvo_d(Lujvo,Number):-
	dictionary_selbri(Lujvo,lujvo,Number,_,_).

%!	fuhivla_data(?Fuhivla,?Arguments)

:- dynamic fuhivla_data/2.

%!	fuhivla_par(?Fuhivla,?Arguments)//

:- dynamic fuhivla_par//2.

%!	fuhivla_d(?Fuhivla,?Arguments)

:- volatile fuhivla_d/2.

fuhivla_d(Fuhivla,Number):-
	dictionary_selbri(Fuhivla,'fu\'ivla',Number,_,_).

:- volatile dictionary_selbri/5.

dictionary_selbri(Word,Type,Number,Rafsi,DEF):-
	dictionary_record(Word,Type,Record),
	findall(R,xpath(Record,rafsi,element(rafsi,[],[R])),Rafsi),
	xpath(Record,definition,element(definition,[],[DEF])),
	re_split("x?\\$[a-z]?_?\\{?\\d+}?",DEF,Split,[]),
	maplist([X,Y]>>(
		    sgml:re_matchsub("x?\\$[a-z]?_?\\{?(?<licz>\\d+)}?",X,W,[]),
		    get_dict(licz,W,Get),
		    number_string(Y,Get),!;
		    Y= 0
		),
		Split,List),
	max_list(List,Number).

%!	dictionary_record(Word,Type,Record)
%
%	It read data from pharsed xml.

:- volatile dictionary_record/3.

dictionary_record(Word,Type,Record):-
	dictionary(Term),
	xpath(Term,//dictionary,Dictionary),!,
	xpath(Dictionary,direction/valsi(@word=Word,@type=Type),Record).

%!	cmavo_optimize
%
%	It reads all cmavo's and assert them to cmavo_data/3 and cmavo_par//1.

:- volatile cmavo_optimize/0.

cmavo_optimize:-
	cmavo_d(Word,Selmaho,Data),
	assert(cmavo_data(Word,Selmaho,Data)),
	atom_codes(Word,Codes),
	expand_term(cmavo_par(Word,Selmaho)-->Codes,LL),
	assert(LL),
	fail;
	compile_predicates([cmavo_data/3,cmavo_par//2]).

%!	cmevla_optimize
%
%	@see cmavo_optimize/0

:- volatile cmevla_optimize/0.

cmevla_optimize:-
	cmevla_d(Word),
	assert(cmevla_data(Word)),
	atom_codes(Word,Codes),
	expand_term((cmevla_par(Word)-->Codes,!),LL),
	assert(LL),
	fail;
	compile_predicates([cmevla_data/1,cmevla_par//1]).

%!	gismu_optimize
%
%	@see cmavo_optimize/0

:- volatile gismu_optimize/0.

gismu_optimize:-
	gismu_d(Gismu,Number,Rafsi),
	assert(gismu_data(Gismu,Number,Rafsi)),
	atom_codes(Gismu,Codes),
	expand_term((gismu_par(Gismu,Number)-->Codes,!),LL),
	assert(LL),
	fail;
	compile_predicates([gismu_data/3,gismu_par//2]).

%!	lujvo_optimize
%
%	@see cmavo_optimize/0

:- volatile lujvo_optimize/0.

lujvo_optimize:-
	lujvo_d(Lujvo,Number),
	assert(lujvo_data(Lujvo,Number)),
	atom_codes(Lujvo,Codes),
	expand_term((lujvo_par(Lujvo,Number)-->Codes,!),LL),
	assert(LL),
	fail;
	compile_predicates([lujvo_data/2,lujvo_par//2]).

%!	fuhivla_optimize
%
%	@see cmavo_optimize/0

:- volatile fuhivla_optimize/0.

fuhivla_optimize:-
	fuhivla_d(Fuhivla,Number),
	assert(fuhivla_data(Fuhivla,Number)),
	atom_codes(Fuhivla,Codes),
	expand_term((fuhivla_par(Fuhivla,Number)-->Codes,!),LL),
	assert(LL),
	fail;
	compile_predicates([fuhivla_data/2,fuhivla_par//2]).

%!	optimize
%
%	It runs all of optimizations.

:- volatile optimize/0.

optimize:-
	cmavo_optimize,
	cmevla_optimize,
	gismu_optimize,
	lujvo_optimize,
	fuhivla_optimize.

:-optimize.
